extends Node

# Memory card scene

var textToSpeech = Global.textToSpeech
var imageWord : String
var id: int
var imagePath : String
var cardBackPath = "res://assets/icons/card.png" # This is the path of the back of the card

signal card_pressed(card)

#Initialize a card with a word
func init_card(word, size, _id):
	if(word == null):
		return
	id = _id
	$backCardImage.set_normal_texture(load(cardBackPath))
	$word.text = word.get_word()
	self.scale_img($backCardImage.texture_normal.get_size(), size)
	self.scale_word(get_node("word").get_combined_minimum_size(), size)
	imagePath = word.get_icon_path()
	imageWord = word.get_word()

##Scale the image to the proper size
func scale_img(realSize, size) :
	if(size.y > size.x) :
		$backCardImage.minimum_size.y = size.y
		$backCardImage.minimum_size.x = size.y * (realSize.x / realSize.y)
		if($backCardImage.minimum_size.x > size.x) :
			$backCardImage.minimum_size.y = (size.x / $backCardImage.minimum_size.x) * $backCardImage.minimum_size.y
			$backCardImage.minimum_size.x = size.x
			#Center the image
			$backCardImage.position.y += (size.y - $backCardImage.minimum_size.y) / 2
		else :
			#Center the image
			$backCardImage.position.x += (size.x - $backCardImage.minimum_size.x) / 2
	else :
		$backCardImage.minimum_size.x = size.x
		$backCardImage.minimum_size.y = size.x * (realSize.y / realSize.x)
		if($backCardImage.minimum_size.y > size.y) :
			$backCardImage.minimum_size.x = (size.y / $backCardImage.minimum_size.y) * $backCardImage.minimum_size.x
			$backCardImage.minimum_size.y = size.y
			#Center the image
			$backCardImage.position.x += (size.x - $backCardImage.minimum_size.x) / 2
		else :
			#Center the image
			$backCardImage.position.y += (size.y - $backCardImage.minimum_size.y) / 2

func scale_word(realSize, size) :
	if(size.y > size.x) :
		$word.minimum_size.y = size.y
		$word.minimum_size.x = size.y * (realSize.x / realSize.y)
		if($word.minimum_size.x > size.x) :
			$word.minimum_size.y = (size.x / $word.minimum_size.x) * $word.minimum_size.y
			$word.minimum_size.x = size.x
			#Center the image
			$word.position.y += (size.y - $word.minimum_size.y) / 2
		else :
			#Center the image
			$word.position.x += (size.x - $word.minimum_size.x) / 2
	else :
		$word.minimum_size.x = size.x
		$word.minimum_size.y = size.x * (realSize.y / realSize.x)
		if($word.minimum_size.y > size.y) :
			$word.minimum_size.x = (size.y / $word.minimum_size.y) * $word.minimum_size.x
			$word.minimum_size.y = size.y
			#Center the image
			$word.position.x += (size.x - $word.minimum_size.x) / 2
		else :
			#Center the image
			$word.position.y += (size.y - $word.minimum_size.y) / 2

func set_texture(img, type):
	if type == "icon":
		if imagePath.length() > 0:
			$backCardImage.set_normal_texture(img)
		else:
			$backCardImage.visible = false
			$word.visible = true
	else:
		$backCardImage.set_normal_texture(img)
		$backCardImage.visible = true
		$word.visible = false

#This function is called when a card is pressed by the user
func _on_backCardImage_pressed():
		emit_signal("card_pressed",self)

func _on_find_card():
	$backCardImage.modulate.a = 0.3
	$word.modulate.a = 0.3
