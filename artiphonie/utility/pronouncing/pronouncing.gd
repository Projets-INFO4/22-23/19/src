extends Control

# Pronouncing scene which display a word in different layout
# Then the user can train by pronouncing the given words
# See the layout under to see what each layout does

var word: Word
var currentLayout: Control
var type : String

signal pronounced(word, result)

func _ready():
	set_process(false)

#_word: Word -> The word we want the user to pronounce
#layout: int -> [1:7] number of the layout we want to use (see below)
#layout = 1 -> sound + picture + borel maisonny sign
#layout = 2 -> sound + borel maisonny signs
#layout = 3 -> sound + picture
#layout = 4 -> sound
#layout = 5 -> picture + borel maisonny signs
#layout = 6 -> picture
#layout = 7 -> borel maisonny signs
#layout = 8 -> nothing
func setup(_word: Word, layout: int = 1, _type : String = 'game'):
	type = _type
	word = _word
	var http_request = HTTPRequest.new()
	add_child(http_request)
	http_request.connect("request_completed",Callable(self,"_http_request_completed"))
	var error = http_request.request("https://static.arasaac.org/pictograms/2628/2628_300.png")
	if error != OK:
		push_error("An error occurred in the HTTP request.")
	
	$backgroundTop/word.text = word.word
	if len(word.iconPath) > 0:
		currentLayout = find_child("layout_" + str(layout))
		currentLayout.visible = true
		extract_borel_maisonny()
		currentLayout.find_child("picture").texture = Global.load_icon(word.iconPath)
	else:
		#Update view if the word don't have an image
		var isBorelNeeded = false
		if layout == 1:
			layout = 2
			isBorelNeeded = true
		elif layout == 3:
			layout = 4
		elif layout == 5:
			layout = 7
			isBorelNeeded = true
		elif layout == 6:
			layout = 8
		currentLayout = find_child("layout_" + str(layout))
		currentLayout.visible = true
		if isBorelNeeded:
			extract_borel_maisonny()

#This function extract all the borel maisonny sign picture out of all the
#phonetic symbol of our function
func extract_borel_maisonny() -> void:
	for imgPath in Global.phonetic_to_array_picture_path(word.phonetic):
		add_borel_maisonny(currentLayout.find_child("borel_maisonny_container"), imgPath)

#This function add a picture of a phonetic symbole in borel maisonny sign to
#a given container. Before adding the picture, the function resize all the other
#picture present in the container.
func add_borel_maisonny(container: HBoxContainer, imgPath: String) -> void:
	var newBorelMaisonny = TextureRect.new()
	newBorelMaisonny.expand = true
	newBorelMaisonny.stretch_mode = newBorelMaisonny.STRETCH_KEEP_ASPECT_CENTERED
	newBorelMaisonny.texture = load(imgPath)
	newBorelMaisonny.size_flags_horizontal = newBorelMaisonny.SIZE_SHRINK_CENTER
	var numberOtherBorelMaisonnyInContainer = container.get_child_count() + 1
	var maxSizeX = container.minimum_size.x/numberOtherBorelMaisonnyInContainer
	var correctSize = min(maxSizeX, container.minimum_size.y)
	newBorelMaisonny.minimum_size.x = correctSize
	newBorelMaisonny.minimum_size.y = correctSize
	for otherBorelMaisonny in container.get_children():
		otherBorelMaisonny.minimum_size.x = correctSize
		otherBorelMaisonny.minimum_size.y = correctSize
	container.add_child(newBorelMaisonny)

func _on_listen_pressed():
	match OS.get_name():
		"Android":
			Global.textToSpeech.textToSpeech(word.word)

func _on_check_pressed():
	if (type == 'train'):
		$correct.playing = true
	Global.player.game_nbAttempts = Global.player.game_nbAttempts +1
	emit_signal("pronounced", word, true)
	pass

func _on_cross_pressed():
	$incorrect.playing = true
	Global.player.game_nbAttempts = Global.player.game_nbAttempts +1
	emit_signal("pronounced", word, false)
	pass

func _on_record_pressed():
	match OS.get_name():
		"Android":
			if not Global.speechToText.isListening():
				currentLayout.find_child("record").modulate = Color(1,1,1,0.5)
				Global.speechToText.stopListen()
				Global.speechToText.doListen()
				set_process(true)
			else:
				currentLayout.find_child("record").modulate = Color(1,1,1,1)
				Global.speechToText.stopListen()
				set_process(false)

#The _process function in this scene is used only for the speech to text,
#basicaly we wait for the user to have said something and when he has said
#something Global.speechToText.isDetectDone() will return true. Then we analyse
#what the user said and we do what we have to do if what the user said is right
#or wrong.
func _process(_delta):
	match OS.get_name():
		"Android":
			if Global.speechToText.isError():
				currentLayout.find_child("record").modulate = Color(1,1,1,0.5)
				Global.speechToText.stopListen()
				Global.speechToText.doListen()
			if Global.speechToText.isDetectDone():
				currentLayout.find_child("record").modulate = Color(1,1,1,1)
				var sentence = Global.speechToText.getWords()
				set_process(false)
				Global.speechToText.stopListen()
				if Global.cmp_string_word(sentence, word):
					#$correct.playing = true
					print("playing")
				else:
					$incorrect.playing = true

func _on_correct_finished():
	#We wait for the sound to finish before signaling of the result
	#emit_signal("pronounced", word, true)
	pass

func _on_incorrect_finished():
	#We wait for the sound to finish before signaling of the result
	#emit_signal("pronounced", word, false)
	pass
