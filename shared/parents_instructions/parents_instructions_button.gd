extends Button

# Parents instructions button change scene to the instructions parents scene with the
# name of the current app as an arguments

# If we want to override the automated way of knowing the current
# app we can specify a special name.
# This is usefull when checked the main menu (which is not an
# app by itself so we have to give the back button its name)
@export var currentAppOverride: String = ""
var currentApp : String #name of the current app played in the global app





func parents_instructions_button_pressed():
	if currentAppOverride == "":
		Global.change_scene_to_file("res://shared/parents_instructions/parents_instructions.tscn", [currentApp])
	else:
		Global.change_scene_to_file("res://shared/parents_instructions/parents_instructions.tscn", [currentAppOverride])
