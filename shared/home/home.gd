extends Control

# The home template is meant to be used to be the home screen of every application

# Allow the list selection button to function
#TODO: Make the list selection button as a separate scene (just like the about button, back button, avatar button, ...)
var listSelectionRessource := load("res://shared/list_selection/list_selection.tscn")

var learnScenePath: String
var trainMenuScenePath: String
var playScenePath: String
var currentApp: String

# _currentApp: String -> Name of the current app (used to be given to the about button)
# The other arguments of setup are self-explanatory
func setup(_learnScenePath: String, _trainMenuScenePath: String, _playScenePath: String, _currentApp: String, titleTexture: Texture2D = null, backgroundTexture: Texture2D = null):
	learnScenePath = _learnScenePath
	trainMenuScenePath = _trainMenuScenePath
	playScenePath = _playScenePath
	$about_button.currentApp = _currentApp
	$parents_instructions_button.currentApp = _currentApp


func learning_button_pressed():
	Global.change_scene_to_file(learnScenePath)

func training_button_pressed():
	Global.change_scene_to_file(trainMenuScenePath)

func playing_button_pressed():
	Global.change_scene_to_file(playScenePath)

func _on_list_change_pressed():
	add_child(listSelectionRessource.instantiate())


