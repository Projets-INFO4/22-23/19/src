extends Control

# Learning scene for Artiphonie

const PHONETIC_PAGE_PATH := "res://artiphonie/learning/phonetic_page/phonetic_page.tscn"

func _ready():
	#deal with the instruction
	var instruction = $Instruction
	instruction.setUp("learning")
	
	var file = FileAccess.open(Global.artiphonie.PATH_PHONETIC_TABLE_SORTED, FileAccess.READ)
	var test_json_conv = JSON.new()
	var p = test_json_conv.parse(file.get_as_text())
	var phoneticType: Dictionary = p.result
	file.close()
	for phoneticTypeName in phoneticType.keys():
		$learning.add_learning_element(phoneticTypeName,
		PHONETIC_PAGE_PATH,
		phoneticType[phoneticTypeName]["iconPath"],
		[phoneticTypeName, phoneticType[phoneticTypeName]["phonetics"]])
