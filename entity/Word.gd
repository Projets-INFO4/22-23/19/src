extends Entity

class_name Word

# The word entity which represent a word duh...

# The phonetic code of the word (this is made following lexique [http://www.lexique.org/] phonetic code)
var phonetic: String : get = get_phonetic, set = set_phonetic
# word is what we would call orthographe in french
var word: String : get = get_word, set = set_word
# The icon of the word can be located checked "res://" or "user://" if dynamically loaded
var iconPath: String : get = get_icon_path, set = set_icon_path

var id : int

func _init():
	id = -1
	phonetic = ""
	word = ""
	iconPath = ""

func set_phonetic(_phonetic : String) -> void:
	phonetic = _phonetic

func get_phonetic() -> String:
	return phonetic

func set_word(_word : String) -> void:
	word = _word

func get_word() -> String:
	return word

func set_id(_id: int) -> void:
	id = _id

func get_id() -> int:
	return id

func set_icon_path(_iconPath : String) -> void:
	iconPath = _iconPath

func get_icon_path() -> String:
	return iconPath


func equals(otherWord: Word) -> bool:
	var result := true
	result = result and otherWord.id == id
	result = result and otherWord.word == word
	result = result and otherWord.phonetic == phonetic
	result = result and otherWord.iconPath == iconPath
	return result


func to_str() -> String : 
	var result :=""
	result += "id: " + str(id) + "\n"
	result += "word: " + word + "\n"
	result += "phonetic: " + phonetic + "\n"
	result += "iconPath: " + iconPath + "\n"
	return result

func to_dictionary() -> Dictionary:
	var result ={}
	result["id"] = id
	result["phonetic"]  = phonetic
	result["word"]  = word
	result["iconPath"]  = iconPath
	return result

func from_dictionary(content: Dictionary) -> Entity:
	#print("test content ", content.has("word"))
	phonetic = content["phonetic"]
	word = content["word"]
	iconPath = content["iconPath"]
	return self

func from_dictionary_new(content: Dictionary) -> Entity:
	id = content["id"]
	phonetic = content["phonem"]
	word = content["orthographe"]
	#var file2Check = File.new()
	#var doFileExists = file2Check.file_exists("res://art/word_icon/"+ content["orthographe"] +".png")
	if Global.imagePresent.has(content["orthographe"]):
		iconPath = "res://art/word_icon/"+ content["orthographe"] +".png"
	else:
		iconPath = ""
	return self
