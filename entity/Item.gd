extends Entity

class_name Item

# The Item entity is the entity for all the items in the shop

# Every item of the same type has to have a different name

var itemName: String : get = get_item_name, set = set_item_name
var required: int : get = get_required, set = set_required
var picturePath: String : get = get_picture_path, set = set_picture_path
# itemType reprensent for example: "hats", "t-shirts", ...
var itemType: String : get = get_item_type, set = set_item_type

func _init():
	itemName = ""
	required = 0
	picturePath = ""
	itemType = ""

func set_item_name(_itemName: String) -> void:
	itemName = _itemName

func get_item_name() -> String:
	return itemName

func set_required(_required: int) -> void:
	required = required

func get_required() -> int:
	return required

func set_picture_path(_picturePath: String) -> void:
	picturePath = _picturePath

func get_picture_path() -> String:
	return picturePath

func set_item_type(_itemType: String) -> void:
	itemType = _itemType

func get_item_type() -> String:
	return itemType


func equals(item: Item) -> bool:
	var result = true
	result = result and item.itemName == itemName
	result = result and item.itemType == itemType
	result = result and item.required == required
	result = result and item.picturePath == picturePath
	return result

func to_str() -> String:
	var result := ""
	result += "itemName: " + itemName + "\n"
	result += "itemType: " + itemName + "\n"
	result += "required: " + str(required) + "\n"
	return result

func to_dictionary() -> Dictionary:
	var result := {}
	result["itemName"] = itemName
	result["required"] = required
	result["picturePath"] = picturePath
	result["itemType"] = itemType
	return result

func from_dictionary(content: Dictionary) -> Entity:
	itemName = content["itemName"]
	required = int(content["required"])
	picturePath = content["picturePath"]
	itemType = content["itemType"]
	return self
