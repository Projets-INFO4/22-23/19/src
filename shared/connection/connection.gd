extends Control


var id:String
var password:String
var dictionnaryResult


# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass


func _on_Button_pressed():
	$HTTP_Error.text = ""
	var data_to_send={"login":id,"password":password}
	var query=JSON.stringify(data_to_send)
	var header= ["Content-Type:application/json","Content-Length: "+str(query.length())]
	$HTTPRequest.request(Global.backendAddr + "/enfants/connection",header,false,HTTPClient.METHOD_POST,query);

func _on_id_text_changed(new_text):
	id = new_text


func _on_password_text_changed(new_text):
	password = new_text


func _on_HTTPRequest_request_completed(result, response_code, headers, body):
	if response_code==200:
		#print(body.get_string_from_utf8())
		Global.player.unlockedItems = []
		var test_json_conv = JSON.new()
		test_json_conv.parse(body.get_string_from_utf8())
		dictionnaryResult = test_json_conv.get_data()
		Global.player.equipedItems = []
		Global.player.set_login(dictionnaryResult["login"])
		Global.player.set_player_name(dictionnaryResult["prenom"])
		Global.player.set_stars(dictionnaryResult["nb_etoile"])
		Global.player.set_idPlayer(dictionnaryResult["id"])
		Global.player.set_isWorking(dictionnaryResult["isWorking"])
		Global.player.set_gender(dictionnaryResult["gender"])
		Global.player.set_ethnicity(dictionnaryResult["ethnicity"])
		Global.player.set_locked_goose_easy(dictionnaryResult["locked_goose_easy"])
		Global.player.set_locked_goose_medium(dictionnaryResult["locked_goose_medium"])
		Global.player.set_locked_goose_hard(dictionnaryResult["locked_goose_hard"])
		Global.player.set_locked_listen_choose_easy(dictionnaryResult["locked_listen_choose_easy"])
		Global.player.set_locked_listen_choose_medium(dictionnaryResult["locked_listen_choose_medium"])
		Global.player.set_locked_listen_choose_hard(dictionnaryResult["locked_listen_choose_hard"])
		Global.player.set_locked_memories_easy(dictionnaryResult["locked_memories_easy"])
		Global.player.set_locked_memories_medium(dictionnaryResult["locked_memories_medium"])
		Global.player.set_locked_memories_hard(dictionnaryResult["locked_memories_hard"])
		Global.listen_horiz = dictionnaryResult["horiz"]
		Global.player.set_Online(true)
		
		var arrayWork = Array()
		var stringWork = dictionnaryResult["instructions"]
		var arrayWorkPool = stringWork.rsplit(",", true)
		for item in arrayWorkPool:
			if item != "[]":
				arrayWork.append(item)
		Global.player.instructionAlreadyPlayed = arrayWork
		print(Global.player.instructionAlreadyPlayed)
		
		Global.change_scene_to_file("res://shared/main_menu/main_menu.tscn")
	elif response_code == 404:
		$HTTP_Error.text = String("Identifiant ou mot de passe incorrect")
		print("Not found")
	else:
		$HTTP_Error.text = String("La connexion au serveur n'a pas pu être établie")
		print("HTTP Error")


func _on_Button2_pressed():
	Global.player.offline_player()
	Global.change_scene_to_file("res://shared/main_menu/main_menu.tscn")
