extends Node

# This scene is called when we launch the application
# Its goal is pretty simple, trying to get the correct permission from
# the android OS. And if that is OK, then it tries to launch the 
# text to speech and the speech to text. In the future, this might change
# as we want to have an offline/supervisor mode which does not 
# relies checked the speech to text.

# It is important to note, that godot needs to restart to enable fully the
# speech to text, we have not found a way around it.

func _ready():
	randomize()
	#var mots = Global.load_only_json("res://data/mots.json")
	var mots = Global.load_only_json("user://mots.json")
	var http_request = HTTPRequest.new()
	add_child(http_request)
	http_request.connect("request_completed",Callable(self,"_http_request_completed"))
	var body = {"bdd_version": 0}
	if mots:
		body = {"bdd_version": int(mots["bdd_version"])}
	
	match OS.get_name():
		"Android":
			var recordAudioPermission := false
			#We go through all the granted permission to see if the one for recording audio is granted
			for grantedPermissions in OS.get_granted_permissions():
				recordAudioPermission = recordAudioPermission or grantedPermissions == "android.permission.RECORD_AUDIO"
			#If not then we request it and wait for the user to accept it
			if !recordAudioPermission:
				OS.request_permission("RECORD_AUDIO")
				set_process(true)
			else:
				print("No permission")
				#if(Engine.has_singleton("GodotSpeech")):
					#Global.speechToText = Engine.get_singleton("GodotSpeech")
				#else: # Error when launching GodotSpeech
					#OS.alert("L'application a rencontré une erreur avec son module \"speech to text\" et va s'arrété dans 10 secondes.", "Erreur")
					#$quit_timer.start()
				#launch()
			if Engine.has_singleton("GodotTTS"):
				Global.textToSpeech = Engine.get_singleton("GodotTTS")
				if Global.textToSpeech.isLanguageAvailable("fr", "FR"):
					Global.textToSpeech.initTextToSpeech("fr", "FR")
					print("Language ready")
				else:
					print("Language not available")
					#Global.textToSpeech.fireTTS()
			else:
				print("Not available")
		#_: #default
			#launch()

func _process(delta):
	match OS.get_name():
		"Android":
			for grantedPermissions in OS.get_granted_permissions():
				if grantedPermissions == "android.permission.RECORD_AUDIO":
					set_process(false)
					OS.alert("L'application va s'arrêter dans 10 secondes pour appliquer la permission. Veuillez le relancer.", "Redémarrage requis")
					$quit_timer.start()
		_: #default
			set_process(false)

func _http_request_completed(response_code, body):
	if response_code == 200:
		var test_json_conv = JSON.new()
		test_json_conv.parse(body.get_string_from_utf8())
		var dictionnaryResult = test_json_conv.get_data()
		if dictionnaryResult:
			if !dictionnaryResult["valid"]:
				#Global.save_only_json(dictionnaryResult["complete"], "res://data/mots.json")
				Global.save_only_json(dictionnaryResult["complete"], "user://mots.json")
	launch()

func launch() -> void:
	#TODO: Make a main menu for all apps and make a first launch scene
	Global.change_scene_to_file("res://shared/connection/connection.tscn")

func _on_quit_timer_timeout() -> void:
	get_tree().quit()
