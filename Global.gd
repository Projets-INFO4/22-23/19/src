extends Node

# Global.gd is the only singleton of our application, it handles most
# of the communication between scenes. And the common function are
# writen here as well.
# The player is accesible here, for saving and getting the active list of word.

# ===== Globals variable specific for main menu =====
#contain the name of the different sub app
var apps : Array = ["artiphonie"]
var currentApp : String
# ===== ===== =====

# ===== Globals specific to an application =====
const Artiphonie := preload("res://shared/artiphonie.gd")
var artiphonie := Artiphonie.new()
# ===== ===== =====

# ===== Scene shown when loading another scene
const loadingScene = preload("res://shared/loading/loading.tscn")

# ===== Useful for the back button =====
var currentScene: int = 0
var scenesChronology := {0: "res://main.tscn"}
var scenesArgumentsChronology := {0: []}
# ===== ===== ====

# ===== Globals variable related to instruction =====
var player: Player = Player.new()
var activeList: Words
# ===== ===== =====

var textToSpeech
var speechToText

var listen_horiz: bool

# ===== Everything related to the phonetic functions =====
const PHONETIC_TABLE_PATH := "res://data/phonetic_table.json"
var phoneticTable: Dictionary
const PHONETIC_TABLE_RESOURCE_PATH := "res://data/phonetic_table_resource.json"
var phoneticTableResource: Dictionary
const PHONETIC_PICTURE_PATH := "res://art/borel_maisony/icon/"
const PHONETIC_PICTURE_EXTENSION := ".png"
const PHONETIC_VIDEO_PATH := "res://art/borel_maisony/video/"
const PHONETIC_VIDEO_EXTENSION := ".ogv"

# ===== ===== =====

# Lexique383 dictionary with ortho as the key and phon as the value
# Used to compare words when one is said
# (for exemple coup, coût sound the same in french so we have to compare their phonetic)
const LEXIQUE_LIGHT_PATH := "res://data/Lexique/Lexique383_ortho_phon_light.json"
var lexiqueLight: Dictionary = {}

# ===== Globals variable available to all application =====
var instructionAlreadyPlayed: Array = []

var imagePresent: Array = []
# ===== ===== =====

var backendAddr = "https://artiphonieback.rakura.fr"
#var backendAddr = "http://localhost:5000" utiliser ça pour le back changer l'adresse + faire tourner la bdd avec docker

func _ready():
	listen_horiz = false
	var file = FileAccess.open(PHONETIC_TABLE_PATH, FileAccess.READ)
	var test_json_conv = JSON.new()
	var stock = test_json_conv.parse(file.get_as_text()) #modified
	phoneticTable = test_json_conv.data
	file.close()

	file = FileAccess.open(PHONETIC_TABLE_RESOURCE_PATH, FileAccess.READ)
	test_json_conv = JSON.new()
	var result = test_json_conv.parse(file.get_as_text())
	phoneticTableResource = test_json_conv.data
	file.close()

	load_json(player, "res://data/general.json")
	activeList = player.listOfWords[0]
	
	imagePresent = load_only_json_array("res://data/images.json")

func get_listen_horiz() -> bool:
	return listen_horiz

func set_listen_horiz(listen: bool):
	listen_horiz = listen

# ===== JSON =====
func load_json(entity: Entity, jsonPath: String) -> Entity:
	var file = FileAccess.open(jsonPath, FileAccess.READ)
	var test_json_conv = JSON.new()
	var result1 = test_json_conv.parse(file.get_as_text()) #modified
	entity.from_dictionary(test_json_conv.data)
	file.close()
	return entity
func load_only_json(jsonPath: String) -> Dictionary:
	var file = FileAccess.open(jsonPath, FileAccess.READ)
	var test_json_conv = JSON.new()
	# test_json_conv.parse(file.get_as_text()).result
	var result1 = test_json_conv.parse(file.get_as_text())
	var ret = test_json_conv.data
	file.close()
	return ret

func load_only_json_array(jsonPath: String) -> Array:
	var file = FileAccess.open(jsonPath, FileAccess.READ)
	var test_json_conv = JSON.new()
	var result1 = test_json_conv.parse(file.get_as_text()) 
	var ret = test_json_conv.data
	file.close()
	return ret

func save_json(entity: Entity, jsonPath: String) -> bool:
	var file = FileAccess.open(jsonPath, FileAccess.WRITE)
	file.store_string(JSON.stringify(entity.to_dictionary(), "\t"))
	file.close()
	return true
	
func save_only_json(dico: Dictionary, jsonPath: String) -> bool:
	var file = FileAccess.open(jsonPath, FileAccess.WRITE)
	file.store_string(JSON.stringify(dico, "\t"))
	file.close()
	return true
# ===== ===== =====

# ===== Scene changement =====
# This function allows to change scene and to give arguments if necessary.
func change_scene_to_file(newScenePath: String, arguments: Array = []) -> void:
	# We switch to the loading scene, this is in case the scene takes a lot of time
	# to load. This is kind of useless, because our scenes are pretty light and
	# most of the loading happens when the scene is launched and allready switched to.
	# So to fix this we could just place the loading scene behind all scene, but I am
	# not sure if it is worth it.
	get_tree().change_scene_to_packed(loadingScene)
	currentScene += 1
	# We had this new scene to our chronology
	scenesChronology[currentScene] = newScenePath
	# We had the arguments to the chronology even there is none
	scenesArgumentsChronology[currentScene] = arguments
	var newSceneRessource = load(newScenePath)
	get_tree().change_scene_to_packed(newSceneRessource)
	
	var mots = Global.load_only_json("user://mots.json")
	var http_request = HTTPRequest.new()
	add_child(http_request)
	http_request.connect("request_completed",Callable(self,"_http_request_completed_check"))
	var body = {"bdd_version": 0}
	if mots:
		body = {"bdd_version": int(mots["bdd_version"])}
	else:
		print("None bdd")
	var query=JSON.stringify(body)
	var error = http_request.request(Global.backendAddr + "/mots/check", [], HTTPClient.METHOD_POST, query)
		
	if Global.player.get_Online() == true && Global.player.saveModification > 0:
		var http_requests = HTTPRequest.new()
		add_child(http_requests)
		http_requests.connect("request_completed",Callable(self,"_http_request_completed"))
		var instructionAlready = PackedStringArray(Global.player.instructionAlreadyPlayed)
		var instructionAlready2 = String(",").join(instructionAlready)

		body = {"gender": int(Global.player.get_gender()),
		"ethnicity": int(Global.player.get_ethnicity()),
		"instructions": String(instructionAlready2),
		"equipedItems": String(""),
		"horiz": bool(Global.listen_horiz),
		"locked_goose_easy": bool(Global.player.locked_goose_easy),
		"locked_goose_medium": bool(Global.player.locked_goose_medium),
		"locked_goose_hard": bool(Global.player.locked_goose_hard),
		"locked_listen_choose_easy": bool(Global.player.locked_listen_choose_easy),
		"locked_listen_choose_medium": bool(Global.player.locked_listen_choose_medium),
		"locked_listen_choose_hard": bool(Global.player.locked_listen_choose_hard),
		"locked_memories_easy": bool(Global.player.locked_memories_easy),
		"locked_memories_medium": bool(Global.player.locked_memories_medium),
		"locked_memories_hard": bool(Global.player.locked_memories_hard),
		"id_enfant": int(Global.player.get_idPlayer())}
		query=JSON.stringify(body)
		error = http_requests.request(Global.backendAddr + "/enfants/update/game", [], HTTPClient.METHOD_POST, query)
		Global.player.saveModification = 0
	#save_json(Global.player, "res://data/general.json")

func _http_request_completed_check(result, response_code, headers, body):
	if response_code == 200:
		var test_json_conv = JSON.new()
		test_json_conv.parse(body.get_string_from_utf8())
		var dictionnaryResult = test_json_conv.get_data()
		if !dictionnaryResult["valid"]:
			print("Update dico")
		if dictionnaryResult:
			if !dictionnaryResult["valid"]:
				#Global.save_only_json(dictionnaryResult["complete"], "res://data/mots.json")
				Global.save_only_json(dictionnaryResult["complete"], "user://mots.json")

func _http_request_completed(result, response_code, headers, body):
	pass

# The scene which was changed to can choose to call get_arguments(),
# if it needed arguments to functions.
func get_arguments() -> Array:
	return scenesArgumentsChronology[currentScene].duplicate(true)

# This function is used primarly by the back button
func change_to_previous_scene() -> void:
	if currentScene == 0:
		return
	get_tree().change_scene_to_packed(loadingScene)
	currentScene -= 1
	var newScene = load(scenesChronology[currentScene])
	get_tree().change_scene_to_packed(newScene)
# ===== ===== =====

# Get N word from the active list
# repeat: bool -> allows to get n >= activeList.size()
#				  But it make the returned list have repeated word
func get_n_word_from_active_list(n: int, repeat: bool = false) -> Array:
	var randomPositions: Array = []
	randomPositions.append(randi() % activeList.words.size())
	for i in range(n-1):
		var tmp = randi() % activeList.words.size()
		if not repeat:
			while tmp in randomPositions:
				tmp = (tmp + 1) % activeList.words.size()
		randomPositions.append(tmp)
	var result: Array = []
	for randomPosition in randomPositions:
		result.append(activeList.words[randomPosition])
	return result
	
func get_n_word_from_dico(n: int, repeat: bool = false) -> Array:
	var randomPositions: Array = []
	var content = load_only_json("user://mots.json")
	var lsWords = []
	var wordLength = 0
	var wordLengthAdd : int = 99999
	var arrayWork = Array()
	if Global.player.get_isWorking():
		var stringWork = Global.player.get_work()
		var arrayWorkPool = stringWork.rsplit(",", true)
		for item in arrayWorkPool:
			arrayWork.append(String(item))
			print(item)
			#arrayWork.append(item)
		wordLength = arrayWork.size()
		print("wordLength")
		wordLengthAdd = 0
	
	for words in content["mots"]:
		if wordLength == wordLengthAdd:
			break
		if Global.player.get_isWorking():
			if arrayWork.has(String(words['id'])):
			#if arrayWork.has(words['orthographe']):
				wordLengthAdd += 1
				lsWords.append(Word.new().from_dictionary_new(words))
				print(words["orthographe"])
		else:
			lsWords.append(Word.new().from_dictionary_new(words))
	
	var result: Array = []
	if Global.player.get_isWorking():
		randomize()
		lsWords.shuffle()
	if lsWords.size() < n:
		repeat = true
	randomPositions.append(randi() % lsWords.size())
	for i in range(n-1):
		var tmp = randi() % lsWords.size()
		if not repeat:
			while tmp in randomPositions:
				tmp = (tmp + 1) % lsWords.size()
		randomPositions.append(tmp)
	for randomPosition in randomPositions:
		result.append(lsWords[randomPosition])
	print(result.size())
	return result

func get_word_training() -> Array:
	var content = load_only_json("user://mots.json")
	var lsWords = []
	var wordLength = 0
	var wordLengthAdd = 0
	var arrayWork = Array()

	var stringWork = Global.player.get_work()
	var arrayWorkPool = stringWork.rsplit(",", true)
	for item in arrayWorkPool:
		arrayWork.append(String(item))
	wordLength = arrayWork.size()
	
	for words in content["mots"]:
		if wordLength == wordLengthAdd:
			break
		if arrayWork.has(String(words['id'])):
			wordLengthAdd += 1
			lsWords.append(Word.new().from_dictionary_new(words))
	return lsWords

# ===== Phonetic =====
#convert the code phonetic of a word into the visual API phonetic code
func convert_phonetic(phonetic: String) -> String:
	var result := ""
	for p in phonetic:
		result += phoneticTable[p]
	return result

#get an array of borelImage which represent the code phonetic
func phonetic_to_array_picture_path(phonetic: String) -> Array:
	var result: Array = []
	for i in range(len(phonetic)):
		if phonetic[i] == "u" && i + 1 < len(phonetic):
			if phonetic[i + 1] == "i":
				result.append(PHONETIC_PICTURE_PATH + "y" + PHONETIC_PICTURE_EXTENSION)
				i += 1
			else:
				result.append(PHONETIC_PICTURE_PATH + phoneticTableResource[phonetic[i]] + PHONETIC_PICTURE_EXTENSION)
		else:
			if phonetic[i] in phoneticTableResource:
				result.append(PHONETIC_PICTURE_PATH + phoneticTableResource[phonetic[i]] + PHONETIC_PICTURE_EXTENSION)
	return result

#get an array of video  which represent the code phonetic
func phonetic_to_array_video_path(phonetic: String) -> Array:
	var result: Array = []
	for i in range(len(phonetic)):
		if phonetic[i] == "u" && i + 1 < len(phonetic):
			if phonetic[i + 1] == "i":
				result.append(PHONETIC_VIDEO_PATH + "y" + PHONETIC_VIDEO_EXTENSION)
				i += 1
			else:
				result.append(PHONETIC_VIDEO_PATH + phoneticTableResource[phonetic[i]] + PHONETIC_VIDEO_EXTENSION)
		else:
			if phoneticTableResource[phonetic[i]] != "":
				result.append(PHONETIC_VIDEO_PATH + phoneticTableResource[phonetic[i]] + PHONETIC_VIDEO_EXTENSION)
	return result

# ===== ===== =====
# Load an icon from the given path
# This function is useless for moment as it returns null,
# if the path leads to nothing. However it could be really usefull
# in the future if we have a word that is missing its picture and
# we want to display a picture saying "missing picture" or just a "?"
func load_icon(path: String) -> Resource:
	var icon = load(path)
	if icon != null:
		return load(path)
	else:
		return null

# ===== ===== =====
# cmp_string_word compare a String to a Word
# This is primarly used when we use the speech to text function, which
# return one word or a full sentence (so we have to split the sentence).
# To check if the said word is the same a our word, we can check its
# "orthographe" but if that fail we have to check if the phonetic of the two
# words are the same. Because when you say for example "coup", the speech to text
# can _recognize "coût" which is not wrong because it is pronounced the same.
func cmp_string_word(sentence: String, word: Word) -> bool:
	sentence = sentence.to_lower()
	var sentenceWords = sentence.split(" ")
	if sentenceWords == null or sentenceWords.size() == 0:
		return false
	for w in sentenceWords:
		if w == word.word:
			return true
		if lexiqueLight.is_empty():
			var file = FileAccess.open(LEXIQUE_LIGHT_PATH, FileAccess.READ)
			var test_json_conv = JSON.new()
			var result1 = test_json_conv.parse(file.get_as_text()) #modified
			lexiqueLight = test_json_conv.data
			file.close()
		if not lexiqueLight.has(w):
			continue
		if lexiqueLight[w] == word.phonetic:
			return true
	return false
