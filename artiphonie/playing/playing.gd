extends Control

# Playing scene for artiphonie
# It loads the different games of artiphonie and their difficulties

func _ready():
	var instruction = $Instruction
	instruction.setUp("gameChoose")

	var gooseGameDifficulties := []
	gooseGameDifficulties.append(["Facile", Global.player.locked_goose_easy, ""])
	gooseGameDifficulties.append(["Normal", Global.player.locked_goose_medium, ""])
	gooseGameDifficulties.append(["Difficile", Global.player.locked_goose_hard, ""])
	$playing.add_playing_element(Global.artiphonie.GOOSE_GAME_NAME, Global.artiphonie.PATH_GOOSE_GAME,
	Global.artiphonie.PATH_GOOSE_GAME_ICON, gooseGameDifficulties)

	var listenAndChooseDifficulties := []
	#listenAndChooseDifficulties.append(["[..] Phonetique", false, ""])
	listenAndChooseDifficulties.append(["Facile", Global.player.locked_listen_choose_easy, ""])
	listenAndChooseDifficulties.append(["Normal", Global.player.locked_listen_choose_medium, ""])
	listenAndChooseDifficulties.append(["Difficile", Global.player.locked_listen_choose_hard, ""])
	$playing.add_playing_element(Global.artiphonie.LISTEN_AND_CHOOSE_GAME_NAME, Global.artiphonie.PATH_LISTEN_AND_CHOOSE_GAME,
	Global.artiphonie.PATH_LISTEN_AND_CHOOSE_GAME_ICON, listenAndChooseDifficulties)

	var memoryDifficulties := []
	memoryDifficulties.append(["Facile", Global.player.locked_memories_easy, ""])
	memoryDifficulties.append(["Normal", Global.player.locked_memories_medium, ""])
	memoryDifficulties.append(["Dificile", Global.player.locked_memories_hard, ""])
	$playing.add_playing_element(Global.artiphonie.MEMORY_NAME, Global.artiphonie.PATH_MEMORY,
	Global.artiphonie.PATH_MEMORY_ICON, memoryDifficulties)

