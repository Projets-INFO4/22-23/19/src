extends Node

# Memory card game scene

const memoryCard := preload("./memory_card.tscn")

var listOfWords : Array
var cards : Array
var selected_cards : Array = []
var nbWord : int 
var nbFound : int = 0
var separationPercentage = 0.02
var nbWrongTry : int = 0
var difficulty : String
var idFiltre : int = -1
var validFind : Dictionary = {}

##Timer
@onready var timer = $Timer
var time = 0

func _ready():
	difficulty = Global.get_arguments()[0]
	match difficulty:
		"Facile":
			nbWord = 4
		"Normal":
			nbWord = 8
		"Difficile":
			nbWord = 10
		_:
			nbWord = 6
			
	if Global.player.get_Online():
		var http_request = HTTPRequest.new()
		add_child(http_request)
		http_request.connect("request_completed",Callable(self,"_http_request_completed_filter"))
		var body = {"id_enfant": int(Global.player.get_idPlayer())}
		var query=JSON.stringify(body)
		var error = http_request.request(Global.backendAddr + "/enfants/work?enfant_id=" + str(Global.player.get_idPlayer()), [], HTTPClient.METHOD_GET, query)
	else:
		#setUp the list of card for this party
		listOfWords = Global.get_n_word_from_dico(nbWord)
		start_game()

func start_game():
	#deal with the instruction
	var instruction = $Instruction
	#if the instruction was played setYp return true
	if(instruction.setUp("memory")):
		#we wait for the player to press "passed" to init the timer
		instruction.connect("on_Pass_Pressed",Callable(self,"initTimer"))
	else:
		#the instruction wasn't played, then the timer is init right away
		initTimer()
	
	var nbCard = nbWord*2
	#setUp the grid of cards
	var gridCard = find_child("GridCards")
	var vectorSizeColumsRows = findColumnsRows(nbCard)
	var nbRow  = vectorSizeColumsRows.y
	var nbCol =  vectorSizeColumsRows.x
	gridCard.columns = nbCol
	#calcul the separation between 2 cards
	var separationX = gridCard.size.x * nbCol * separationPercentage
	var separationY = gridCard.size.y * nbRow * separationPercentage
	#calcul the card size depending checked the page size
	var card_size = Vector2((gridCard.size.x - separationX)/ nbCol, (gridCard.size.y - separationY) / nbRow)
	
	gridCard.set("custom_constants/v_separation", card_size.y + (separationY / nbRow))
	gridCard.set("custom_constants/h_separation", card_size.x + (separationX / nbCol))
	
	#Creation of the memory card
	for i in range(0, 2): #create 2 cards for each word
		for w in listOfWords: # For each words
			var card = memoryCard.instantiate()
			card.init_card(w, card_size, w.id)
			#when card is pressed, called the _on_card_pressed function 
			card.connect("card_pressed",Callable(self,"_on_card_pressed"))
			cards.append(card)
	cards.shuffle() # We put the cards in a random order
	for c in cards: # We add the cards to the grid
		gridCard.add_child(c)

func _http_request_completed_filter(result, response_code, headers, body):
	if response_code==200:
		var test_json_conv = JSON.new()
		test_json_conv.parse(body.get_string_from_utf8())
		var dictionnaryResult = test_json_conv.get_data()
		Global.player.set_work(String(dictionnaryResult["wordList"]))
		Global.player.set_isWorking(true)
		idFiltre = dictionnaryResult["id"]
		listOfWords = Global.get_n_word_from_dico(nbWord)
	else:
		Global.player.set_isWorking(false)
		listOfWords = Global.get_n_word_from_dico(nbWord)
	start_game()	
	
func _on_card_pressed(card):
	if(selected_cards.size() < 2 and !selected_cards.has(card)):
		if !(card.id in validFind):
			#print(card.imageWord)
			validFind[card.id] = 0
		selected_cards.append(card) # Add the card to the array
		if(card.textToSpeech != null):
			card.textToSpeech.textToSpeech(card.imageWord) # Tell the word checked the card
		card.set_texture(Global.load_icon(card.imagePath), "icon") # Reveal the image of the card
	else:
		#only one card is selected or the card selected was aleady selected
		return

	if(selected_cards.size() == 2):
		var correct = selected_cards[0].imageWord == selected_cards[1].imageWord
		#show the two card for 1 second
		var t = Timer.new()
		t.set_wait_time(1)
		t.set_one_shot(true)
		self.add_child(t)
		t.start() # Wait for 1 second
		await t.timeout
		if(correct): # If the two cards have the same image
			nbFound += 1
#			Global.player.setSilver(Global.player.getSilver() + 1)
			#selected_cards[0].get_node("backCardImage").visible = false # Hide the two cards
			#selected_cards[1].get_node("backCardImage").visible = false
			#selected_cards[0].get_node("word").visible = false
			#selected_cards[1].get_node("word").visible = false
			selected_cards[0]._on_find_card()
			selected_cards[1]._on_find_card()
			if(nbFound == nbWord): # The game is finished
				game_end()
		else: # If the two cards aren't the same image
			validFind[selected_cards[0].id] = int(validFind[selected_cards[0].id]) + 1
			nbWrongTry +=1
			for c in selected_cards:
				c.set_texture(load(c.cardBackPath), "hide") # Rehide the cards
		selected_cards.clear()


#Calculat the number of col and row depending of the nuber of cards
func findColumnsRows(size : int) :
	var boolean = true
	var restmp
	for i in range (2, 7):
		for j in range (2, 5):
			if(size % i == 0 && size / i == j):
				return Vector2(i, j)
			if(i * j > size && boolean):
				boolean = false
				restmp = Vector2(i, j)
	return restmp

func initTimer():
	timer.start()

func calculate_score():
	var score : int
	if(nbWrongTry <= nbWord/2):
		score = 5
	elif(nbWrongTry <= nbWord):
		score = 4
	elif (nbWrongTry <= nbWord+nbWord/2):
		score = 3
	elif (nbWrongTry <= nbWord+nbWord):
		score = 2
	else: 
		score = 1
	match difficulty:
		"Normal":
			score+=2
		"Difficile":
			score+=4
	return score
	
func game_end():
	Global.player.saveModification += 1
	var score = calculate_score()
	#args to send to game_end
	var args : Array = []
	args.append("res://artiphonie/memory/memory_game.tscn")
	args.append([difficulty])
	args.append("Memory")
	args.append(difficulty)
	args.append(score)
	args.append(time)
	args.append(validFind)
	args.append(idFiltre)
	args.append(unlock_difficulty())
	Global.change_scene_to_file("res://shared/game_end/game_end.tscn", args)


func _on_Timer_timeout():
	time += 1
	updateTimer()

func updateTimer():
	find_child("TimerMinute").text = str(time/60)+" Minutes"
	find_child("TimerSeconde").text = str(time%60)+" Secondes"

func unlock_difficulty():
	var difficulty_to_unlock : String
	var success_percentage : float = float(nbWord) / float(float(nbFound) + float(nbWrongTry))
	match difficulty:
		"Facile":
			difficulty_to_unlock = "Normal"
		"Normal":
			difficulty_to_unlock = "Difficile"
	var unlocksomething = false
	if success_percentage >= 0.6:
		match difficulty_to_unlock:
			"Normal":
				if Global.player.locked_memories_medium:
					Global.player.locked_memories_medium = false
					unlocksomething = true
			"Difficile":
				if Global.player.locked_memories_hard:
					Global.player.locked_memories_hard = false
					unlocksomething = true
	return {"unlock": unlocksomething, "pourcentage": success_percentage}
