extends Entity

class_name Words

# The words entity is the entity representing a list of words
# The name words is poorly choosen sorry

# Name of the list
var listName: String : get = get_list_name, set = set_list_name
# Path3D to the icon of the list (the path could be the same as a word)
var listIconPath: String : get = get_list_icon_path, set = set_list_icon_path
# An array of all the word in the list
var words: Array : get = get_words, set = set_words

func _init():
	listName = ""
	listIconPath = ""
	words = []

func set_list_name(_listName: String) -> void:
	listName = _listName

func get_list_name() -> String:
	return listName

func set_list_icon_path(_listIconPath: String) -> void:
	listIconPath = _listIconPath

func get_list_icon_path() -> String:
	return listIconPath

func set_words(_words: Array) -> void:
	words = _words

func get_words() -> Array:
	return words.duplicate(true)

func add_word(word: Word) -> bool:
	for w in words:
		if word.equals(w):
			return false
	words.append(word)
	return true

func erase_word(word: Word) -> bool:
	for i in range(words.size()):
		if word.equals(words[i]):
			return remove_word(i)
	return false

func remove_word(position: int) -> bool:
	if position < 0 or position >= words.size():
		return false
	words.remove_at(position)
	return true

func equals(otherWords: Words) -> bool:
	var result := true
	result = result and otherWords.listName == listName
	result = result and otherWords.listIconPath == listIconPath
	return result

func to_str() -> String:
	var result := ""
	result += "listName: " + listName + "\n"
	result += "==== words ====\n"
	for word in words:
		result += word.to_str() + "==== ==== ====\n"
	return result

func to_dictionary() -> Dictionary:
	var result := {}
	result["listName"] = listName
	result["listIconPath"] = listIconPath
	result["words"] = []
	for word in words:
		result["words"].append(word.to_dictionary())
	return result

func from_dictionary(content: Dictionary) -> Entity:
	listName = content["listName"]
	listIconPath = content["listIconPath"]
	words = []
	var i = 0
	for word in content["words"].size():
		i= i+1
		words.append(Word.new().from_dictionary(content["words"][word]))
	return self
