extends Control

# The about scene is pretty self-explanatory, it show the about information
# related to the current app

func _ready():
	find_child("TextParentsInstructions").set_text(loadParentsInstructionsContent())
	find_child("TextParentsInstructions").set_selection_enabled(true)

# This load the content of the parents instructions section, to know which content
# should be loaded it takes the arguments given when the scene changed
# which means when the about button pressed (see parents_instructions.tscn/.gd)
func loadParentsInstructionsContent() -> String:
	var currentApp: String = Global.get_arguments()[0]
	if(currentApp != null):
		var file = FileAccess.open("res://data/artiphonie/parentsInstructions.txt", FileAccess.READ)
		var content = file.get_as_text()
		file.close()
		return content
	else:
		return ""
