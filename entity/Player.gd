extends Entity

class_name Player

# The player entity is what is used when using the app
# By default if a player is not logged in Global.gd will load a default player where nothing can be saved (data/default.json)

var playerName: String : get = get_player_name, set = set_player_name

var idPlayer: int
var login : String
var online: bool
var saveModification: int
var isWorking: bool
var work: String

var stars: int : get = get_stars, set = set_stars
# Determines the skin color of the player
var ethnicity: int : get = get_ethnicity, set = set_ethnicity
# 1 == Male | 2 == Female
var gender: int : get = get_gender, set = set_gender
# If the player wants to see the instruction checked every screen
var instruction: bool : get = get_instruction, set = set_instruction
# The equiped items shown checked his avatar
var equipedItems: Array : get = get_equiped_items, set = set_equiped_items
# All the items that the player has bought/unlocked, the equiped items are supposed to be in it as well
var unlockedItems: Array : get = get_unlocked_items, set = set_unlocked_items
# All the list of word a player can choose to use (for example: liste monosyllabique, liste bisyllabique, ...)
var listOfWords: Array : get = get_list_of_words, set = set_list_of_words
# Where to save the player entity when a modification is done
var playerPath: String : get = get_player_path, set = set_player_path
# True if the easy level of goose game is locked, otherwise false
var locked_goose_easy: bool : get = get_locked_goose_easy, set = set_locked_goose_easy
# True if the medium level of goose game is locked, otherwise false
var locked_goose_medium: bool : get = get_locked_goose_medium, set = set_locked_goose_medium
# True if the hard level of goose game is locked, otherwise false
var locked_goose_hard: bool : get = get_locked_goose_hard, set = set_locked_goose_hard
# True if the easy level of listen and choose game is locked, otherwise false
var locked_listen_choose_easy: bool : get = get_locked_listen_choose_easy, set = set_locked_listen_choose_easy
# True if the medium level of listen and choose game is locked, otherwise false
var locked_listen_choose_medium: bool : get = get_locked_listen_choose_medium, set = set_locked_listen_choose_medium
# True if the hard level of listen and choose game is locked, otherwise false
var locked_listen_choose_hard: bool : get = get_locked_listen_choose_hard, set = set_locked_listen_choose_hard
# True if the easy level of memories game is locked, otherwise false
var locked_memories_easy: bool : get = get_locked_listen_choose_easy, set = set_locked_listen_choose_easy
# True if the medium level of memories game is locked, otherwise false
var locked_memories_medium: bool : get = get_locked_memories_medium, set = set_locked_memories_medium
# True if the hard level of listen and choose game is locked, otherwise false
var locked_memories_hard: bool : get = get_locked_memories_hard, set = set_locked_memories_hard
# We have to store the score of the goose game here due to the organisation of the game code
var game_nbAttempts: int : get = get_game_nbAttempts, set = set_game_nbAttempts
# ===== Globals variable available to all application =====
var instructionAlreadyPlayed: Array = []

func _init():
	playerName = "Invité"
	login = ""
	online = false
	saveModification = 0
	idPlayer = -1
	stars = 0
	isWorking = false
	work = ""
	ethnicity = 1
	gender = 1
	instruction = true
	equipedItems = []
	unlockedItems = []
	listOfWords = []
	game_nbAttempts = 0
	playerPath = ""
	locked_goose_easy = false
	locked_goose_medium = true
	locked_goose_hard = true
	locked_listen_choose_easy = false
	locked_listen_choose_medium = true
	locked_listen_choose_hard = true
	locked_memories_easy = false
	locked_memories_medium = true
	locked_memories_hard = true

func offline_player() -> void:
	playerName = "Invité"
	login = ""
	online = false
	isWorking = false
	work = ""
	idPlayer = -1
	stars = 0
	ethnicity = 1
	gender = 1
	instruction = true
	equipedItems = []
	unlockedItems = []
	instructionAlreadyPlayed = []
	game_nbAttempts = 0
	playerPath = ""
	locked_goose_easy = false
	locked_goose_medium = true
	locked_goose_hard = true
	locked_listen_choose_easy = false
	locked_listen_choose_medium = true
	locked_listen_choose_hard = true
	locked_memories_easy = false
	locked_memories_medium = true
	locked_memories_hard = true

func set_player_name(_playerName: String) -> void:
	playerName = _playerName

func set_Online(_online: bool) -> void:
	online = _online

func get_Online() -> bool:
	return online

func set_isWorking(_isWorking: bool) -> void:
	isWorking = _isWorking

func get_isWorking() -> bool:
	return isWorking

func set_work(_work: String) -> void:
	work = _work

func get_work() -> String:
	return work

func get_player_name() -> String:
	return playerName

func set_login(_login: String) -> void:
	login = _login

func get_login() -> String:
	return login

func get_idPlayer() -> int:
	return idPlayer

func set_idPlayer(_id: int) -> void:
	idPlayer = _id

func set_stars(_stars: int) -> void:
	stars = _stars

func get_stars() -> int:
	return stars

func set_ethnicity(_ethnicity: int) -> void:
	ethnicity = _ethnicity
	saveModification += 1

func get_ethnicity() -> int:
	return ethnicity

func set_gender(_gender: int) -> void:
	gender = _gender
	saveModification += 1

func get_gender() -> int:
	return gender

func set_instruction(_instruction: bool) -> void:
	instruction = _instruction
	saveModification += 1

func get_instruction() -> bool:
	return instruction

func set_equiped_items(_equipedItems: Array) -> void:
	equipedItems = _equipedItems


func get_equiped_items() -> Array:
	return equipedItems.duplicate(true)

func add_equiped_item(item: Item) -> bool:
	for i in equipedItems:
		if item.equals(i):
			return false
		elif item.itemType == i.itemType:
			erase_equiped_item(i)
	equipedItems.append(item)
	return true

func remove_equiped_item(position: int) -> bool:
	if position < 0 or position >= equipedItems.size():
		return false
	equipedItems.remove_at(position)
	return true

func erase_equiped_item(item: Item) -> bool:
	for i in range(equipedItems.size()):
		if item.equals(equipedItems[i]):
			return remove_equiped_item(i)
	return false

func has_equiped_item(item: Item) -> bool:
	for i in equipedItems:
		if item.equals(i):
			return true
	return false

func set_unlocked_items(_unlockedItems: Array) -> void:
	unlockedItems = _unlockedItems

func get_unlocked_items() -> Array:
	return unlockedItems.duplicate(true)


func add_unlocked_item(item: Item) -> bool:
	for i in unlockedItems:
		if item.equals(i):
			return false
	unlockedItems.append(item)
	return true

func remove_unlocked_item(position: int) -> bool:
	if position < 0 or position >= unlockedItems.size():
		return false
	unlockedItems.remove_at(position)
	return true

func erase_unlocked_item(item: Item) -> bool:
	for i in range(unlockedItems.size()):
		if item.equals(unlockedItems[i]):
			return remove_unlocked_item(i)
	return false

func set_list_of_words(_listOfWords: Array) -> void:
	listOfWords = _listOfWords
	print("test1 ", _listOfWords)
	print("test2 ", listOfWords)

func get_list_of_words() -> Array:
	return listOfWords.duplicate(true)

func add_words(words: Words) -> bool:
	for w in listOfWords:
		if words.equals(w):
			return false
	listOfWords.append(words)
	return true

func remove_words(position: int) -> bool:
	if position < 0 or position >= listOfWords.size():
		return false
	listOfWords.remove_at(position)
	return true

func erase_list_of_words(words: Words) -> bool:
	for i in range(listOfWords.size()):
		if words.equals(listOfWords[i]):
			return remove_words(i)
	return true

func set_player_path(_playerPath: String) -> void:
	playerPath = _playerPath

func get_player_path() -> String:
	return playerPath

func set_locked_goose_easy(_locked_goose_easy: bool) -> void:
	locked_goose_easy = _locked_goose_easy
	saveModification += 1

func get_locked_goose_easy() -> bool:
	return locked_goose_easy

func set_locked_goose_medium(_locked_goose_medium: bool) -> void:
	locked_goose_medium = _locked_goose_medium
	saveModification += 1

func get_locked_goose_medium() -> bool:
	return locked_goose_medium

func set_locked_goose_hard(_locked_goose_hard: bool) -> void:
	locked_goose_hard = _locked_goose_hard
	saveModification += 1

func get_locked_goose_hard() -> bool:
	return locked_goose_hard

func set_locked_listen_choose_easy(_locked_listen_choose_easy: bool) -> void:
	locked_listen_choose_easy = _locked_listen_choose_easy

func get_locked_listen_choose_easy() -> bool:
	return locked_listen_choose_easy

func set_locked_listen_choose_medium(_locked_listen_choose_medium: bool) -> void:
	locked_listen_choose_medium = _locked_listen_choose_medium

func get_locked_listen_choose_medium() -> bool:
	return locked_listen_choose_medium

func set_locked_listen_choose_hard(_locked_listen_choose_hard: bool) -> void:
	locked_listen_choose_hard = _locked_listen_choose_hard

func get_locked_listen_choose_hard() -> bool:
	return locked_listen_choose_hard

func set_locked_memories_easy(_locked_memories_easy: bool) -> void:
	locked_memories_easy = _locked_memories_easy

func get_locked_memories_easy() -> bool:
	return locked_memories_easy

func set_locked_memories_medium(_locked_memories_medium: bool) -> void:
	locked_memories_medium = _locked_memories_medium

func get_locked_memories_medium() -> bool:
	return locked_memories_medium

func set_locked_memories_hard(_locked_memories_hard: bool) -> void:
	locked_memories_hard = _locked_memories_hard

func get_locked_memories_hard() -> bool:
	return locked_memories_hard

func set_game_nbAttempts(_game_nbAttempts)-> void:
	game_nbAttempts = _game_nbAttempts

func get_game_nbAttempts() -> int:
	return game_nbAttempts

func to_str() -> String:
	var result := ""
	result += "playerName: " + playerName + "\n"
	result += "stars: " + str(stars) + "\n"
	result += "ethnicity: " + str(ethnicity) + "\n"
	result += "gender: " + str(gender) + "\n"
	result += "instruction: " + str(instruction) + "\n"
	result += "==== equipedItems ==== \n"
	for item in equipedItems:
		result += item.to_str()
		result += "==== ==== ====\n"
	result += "==== unlockedItems ==== \n"
	for item in unlockedItems:
		result += item.to_str()
		result += "==== ==== ====\n"
	result += "==== listOfWords ==== \n"
	for words in listOfWords:
		result += words.to_str()
		result += "==== ==== ==== \n"
	return result

func to_dictionary() -> Dictionary:
	var result := {}
	result["playerName"] = playerName
	result["stars"] = stars
	result["ethnicity"] = ethnicity
	result["gender"] = gender
	result["instruction"] = instruction
	result["playerPath"] = playerPath
	result["equipedItems"] = []
	result["listOfWords"] = []
	result["unlockedItems"] = []
	result["locked_goose_easy"] = locked_goose_easy
	result["locked_goose_medium"] = locked_goose_medium
	result["locked_goose_hard"] = locked_goose_hard
	result["locked_listen_choose_easy"] = locked_listen_choose_easy
	result["locked_listen_choose_medium"] = locked_listen_choose_medium
	result["locked_listen_choose_hard"] = locked_listen_choose_hard
	result["locked_memories_easy"] = locked_memories_easy
	result["locked_memories_medium"] = locked_memories_medium
	result["locked_memories_hard"] = locked_memories_hard
	for item in equipedItems:
		result["equipedItems"].append(item.to_dictionary())
	for item in unlockedItems:
		result["unlockedItems"].append(item.to_dictionary())
	for words in listOfWords:
		result["listOfWords"].append(words.to_dictionary())
	print("ent", listOfWords)
	return result

func from_dictionary(content: Dictionary) -> Entity:
	playerName = content["playerName"]
	stars = content["stars"]
	ethnicity = content["ethnicity"]
	gender = content["gender"]
	instruction = content["instruction"]
	playerPath = content["playerPath"]
	locked_goose_easy = content["locked_goose_easy"]
	locked_goose_medium = content["locked_goose_medium"]
	locked_goose_hard = content["locked_goose_hard"]
	locked_listen_choose_easy = content["locked_listen_choose_easy"]
	locked_listen_choose_medium = content["locked_listen_choose_medium"]
	locked_listen_choose_hard = content["locked_listen_choose_hard"]
	locked_memories_easy = content["locked_memories_easy"]
	locked_memories_medium = content["locked_memories_medium"]
	locked_memories_hard = content["locked_memories_hard"]
	equipedItems = []
	for item in content["equipedItems"]:
		equipedItems.append(Item.new().from_dictionary(item))
	unlockedItems = []
	for item in content["unlockedItems"]:
		unlockedItems.append(Item.new().from_dictionary(item))
	listOfWords = []
	for words in content["listOfWords"]:
		listOfWords.append(Words.new().from_dictionary(words))
		print("words are ", words)
	print("list is ", listOfWords)
	return self
