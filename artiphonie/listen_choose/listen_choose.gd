extends Control

# Listen and choose scene

var CardSceneRessource = preload("res://artiphonie/listen_choose/card_listen_choose.tscn")
var CardSceneRessourceHoriz = preload("res://artiphonie/listen_choose/card_listen_choose_horiz.tscn")

const NB_SCREEN_MAX = 10

var sizeCard : Vector2

var listOfUndoneWords : Array
var listOfUndoneSounds : Array

var cardToFind : Word
var nb_screen : int = 0
var nbRight : int = 0
var difficulty : String
var cardLayout : int
var content = Global.load_only_json("user://mots.json")

var validFind : Dictionary = {}
var timer: int = 0

var idFiltre : int = -1

var horiz: bool = Global.listen_horiz

func _ready():
	#deal with the instruction
	var instruction = $Instruction
	instruction.setUp("listen_choose")

	var arguments = Global.get_arguments()
	difficulty = arguments[0]
	
	match difficulty:
		"[..] Phonetique":
			horiz = false
			cardLayout = 0
			listOfUndoneWords = get_phonetic_list()
		"Facile":
			cardLayout = 1
			listOfUndoneWords = content["mots"]
		"Normal":
			cardLayout = 2
			listOfUndoneWords = content["mots"]
		"Difficile":
			cardLayout = 3
			listOfUndoneWords = content["mots"]
			
	if horiz:
		#set up of the size of the cards
		var sizeScene = Vector2(self.size.x,self.size.y)
		sizeCard = Vector2(sizeScene.x*0.6, sizeScene.y/4.4)
		$cardGrid_horiz.visible = true
	else:
		#set up of the size of the cards
		var sizeScene = Vector2(self.size.x,self.size.y)
		sizeCard = Vector2(sizeScene.x/3, sizeScene.y*0.6)
		$cardGrid.visible = true
	
	if Global.player.get_Online():
		var http_request = HTTPRequest.new()
		add_child(http_request)
		http_request.connect("request_completed",Callable(self,"_http_request_completed_filter"))
		var body = {"id_enfant": int(Global.player.get_idPlayer())}
		var query=JSON.stringify(body)
		var error = http_request.request(Global.backendAddr + "/enfants/work?enfant_id=" + str(Global.player.get_idPlayer()), [], HTTPClient.METHOD_GET, query)
	else:
		next_screen()

func _http_request_completed_filter(result, response_code, headers, body):
	if response_code==200:
		var test_json_conv = JSON.new()
		test_json_conv.parse(body.get_string_from_utf8())
		var dictionnaryResult = test_json_conv.get_data()
		Global.player.set_work(String(dictionnaryResult["wordList"]))
		Global.player.set_isWorking(true)
		idFiltre = dictionnaryResult["id"]
		if difficulty != "[..] Phonetique":
			listOfUndoneWords = Global.get_word_training()
	else:
		Global.player.set_isWorking(false)
	next_screen()

func levenshtein(m1 : String, m2 : String) -> int:
	m1 = m1.to_lower()
	m2 = m2.to_lower()
	var m:int = len(m1)
	var n:int = len(m2)
	var d: Array = []
	for i in range(1, m+1):
		d.append([i])
	d.insert(0, range(0, n+1))
	for j in range(1, n+1):
		for i in range(1, m+1):
			var substitution_cost: int
			if m1[i-1] == m2[j-1]:
				substitution_cost = 0
			else:
				substitution_cost = 1
			d[i].insert(j, min(min(
				d[i-1][j]+1,
				d[i][j-1]+1),
				d[i-1][j-1]+substitution_cost
			))
	return d[-1][-1]

func next_screen():
	var currentCard
	#the game continue while we all the screen haven't been done
	#bt while there are still enough word available in listOfUndoneWords
	if(nb_screen < NB_SCREEN_MAX && listOfUndoneWords.size() > 0):
		var wordsToShow = []
		var cardAlea = randi() % 3
		if difficulty != "[..] Phonetique":
			var wordToFind = get_n_word_from_given_list(listOfUndoneWords, 1)
			
			if !Global.player.isWorking:
				wordToFind[0] = Word.new().from_dictionary_new(wordToFind[0])
				
			print(wordToFind[0].word)
			
			listOfUndoneWords.erase(wordToFind[0])
			
			var pretendWords = {1: [], 2: [], 3: [], 4: [], 5: []}
			var wordLen = 1
			for words in content["mots"]:
				if words["orthographe"] != wordToFind[0].word && len(words["orthographe"]) >= len(wordToFind[0].word) - wordLen && len(words["orthographe"]) <= len(wordToFind[0].word) + wordLen:
					var lenvent = levenshtein(wordToFind[0].phonetic, words["phonem"])
					if lenvent == 1 || lenvent == 2 || lenvent == 3 || lenvent == 4 || lenvent == 5:
						pretendWords[lenvent].append(Word.new().from_dictionary_new(words))
			var arrPretend = [1, 2, 3, 4, 5]
			
			for pret in arrPretend:
				if wordsToShow.size() == 2:
					break
				if wordsToShow.size() < 2 && pretendWords[pret].size() > 0:
					for word in pretendWords[pret]:
						if wordsToShow.size() == 2:
							break;
						else:
							wordsToShow.append(word)
				
			if wordsToShow.size() != 2:
				var add = 2 - wordsToShow.size()
				var getWord = []
				if listOfUndoneWords.size() > 1:
					getWord = get_n_word_from_given_list(listOfUndoneWords, add, wordToFind[0].word)
				else:
					getWord = Global.get_n_word_from_dico(add)
				for word in getWord:
					if !Global.player.isWorking:
						word = Word.new().from_dictionary_new(word)
					print(word.word)
					wordsToShow.append(word)
			var wordFDic = wordToFind[0]
			wordsToShow.append(wordFDic)
			randomize()
			wordsToShow.shuffle()
			cardAlea = wordsToShow.find(wordFDic)
		else:
			wordsToShow = get_n_word_from_given_list(listOfUndoneWords,3)
		
		#for the 3 cards that will be displayed
		for i in range (0,3):
			currentCard = CardSceneRessource.instantiate()
			if horiz:
				currentCard = CardSceneRessourceHoriz.instantiate()
			currentCard.name = str(i)
			if horiz:
				find_child("cardGrid_horiz").add_child(currentCard)
			else:
				find_child("cardGrid").add_child(currentCard)
			currentCard.setUpCard(wordsToShow[i], cardLayout, sizeCard, horiz)
			currentCard.connect("card_pressed",Callable(self,"_on_Validate_pressed"))
			
		
		#choose randomly the card to find
		cardToFind = wordsToShow[cardAlea]
		if difficulty == "[..] Phonetique":
			validFind[cardToFind.word] = 0
		else:
			validFind[cardToFind.id] = 0
		#delete the word to find to not choose it again
		nb_screen +=1
	else :
		game_end()

#tell the word to find when the button is pressed
func _on_speak_pressed():
	if difficulty == "[..] Phonetique":
		$Sound.stream = load(str(Global.PHONETIC_VIDEO_PATH+cardToFind.word+Global.PHONETIC_VIDEO_EXTENSION))
		$Sound.play()
	else:
		match OS.get_name():
			"Android":
				Global.textToSpeech.textToSpeech(cardToFind.word)

#is called when a card is pressed
func _on_Validate_pressed(card):
	if(cardToFind.word == card.word.word):
		if difficulty == "[..] Phonetique":
			validFind[cardToFind.word] = 0
		else:
			validFind[cardToFind.id] = 1
		nbRight += 1
		find_child("Good").playing = true
	else :
		find_child("Wrong").playing = true
	if horiz:
		for i in range(0, $cardGrid_horiz.get_child_count()):
			$cardGrid_horiz.get_child(i).queue_free()
	else:
		for i in range(0, $cardGrid.get_child_count()):
			$cardGrid.get_child(i).queue_free()
	next_screen()

#return n word from the fiven list
func get_n_word_from_given_list(list : Array , n: int, word : String = "") -> Array:
	var randomPositions: Array = []
	randomPositions.append(randi() % list.size())
	for i in range(n-1):
		var find = false
		var tmp = randi() % list.size()
		while !find:
			tmp = (tmp + 1) % list.size()
			var check = list[tmp]
			if typeof(list[tmp]) != 17:
				check = Word.new().from_dictionary_new(list[tmp])
			if len(word) == 0 && !(tmp in randomPositions) || len(word) != 0 && !(tmp in randomPositions) && check.word != word:
				find = true
				randomPositions.append(tmp)
	var result: Array = []
	for randomPosition in randomPositions:
		result.append(list[randomPosition])
	return result

func calculate_score():
	var score = (5 * nbRight)/nb_screen
	if score == 0:
		score+=1
	match difficulty:
		"Normal":
			score+=2
		"Difficile":
			score+=4
	return score

func game_end():
	Global.player.saveModification += 1
	var score = calculate_score()
	#args to send to game_end
	var args : Array = []
	args.append("res://artiphonie/listen_choose/listen_choose.tscn")
	args.append([difficulty])
	args.append("Ecoute et choisis")
	args.append(difficulty)
	args.append(score)
	args.append(timer)
	args.append(validFind)
	args.append(idFiltre)
	args.append(unlock_difficulty())
	Global.change_scene_to_file("res://shared/game_end/game_end.tscn", args)

func unlock_difficulty():
	var success_percentage : float = float(nbRight) / float(nb_screen)
	var difficulty_to_unlock : String
	match difficulty:
		"Facile":
			difficulty_to_unlock = "Normal"
		"Normal":
			difficulty_to_unlock = "Difficile"
	var unlocksomething = false
	print(success_percentage)
	if success_percentage >= 0.8:
		match difficulty_to_unlock:
			"Normal":
				if Global.player.locked_listen_choose_medium:
					Global.player.locked_listen_choose_medium = false
					unlocksomething = true
			"Difficile":
				if Global.player.locked_listen_choose_hard:
					Global.player.locked_listen_choose_hard = false
					unlocksomething = true
	return {"unlock": unlocksomething, "pourcentage": success_percentage}

func get_phonetic_list() -> Array:
	var dictionary = Global.phoneticTableResource.duplicate(true)
	var listOfSound : Array = []
	for sound in dictionary.keys():
		if dictionary[sound] != "":
			var w = Word.new()
			w.word = dictionary[sound]
			w.phonetic = sound
			w.iconPath = str("res://art/borel_maisony/icon/"+w.word+".png")
			listOfSound.append(w)
	return listOfSound
