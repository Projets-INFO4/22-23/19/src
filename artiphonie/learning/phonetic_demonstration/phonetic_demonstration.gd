extends Control

# Phonetic page is similar to pronouncing, it display a phonetic symbole
# , its borel-maisony picture and a video of the gesture being made

func setup(phonetic: String, picturePath: String, videoPath: String) -> void:
	$background/title.text = "[" + Global.convert_phonetic(phonetic) + "]"
	var piLoad = load(picturePath)
	var viLoad = load(videoPath)
	if piLoad != null:
		$background/picture.texture = piLoad
	if viLoad != null:
		$background/video_frame/video.stream = viLoad
		$background/video_frame/video.play()
		scale_center_video()

func scale_center_video() -> void:
	var videoTexture: Texture2D = $background/video_frame/video.get_video_texture()
	var videoSize = Vector2(videoTexture.get_width(), videoTexture.get_height())
	$background/video_frame/video.size = videoSize
	var scale := 1.0
	if max(videoSize.x, videoSize.y) == videoSize.x:
		scale = $background/video_frame/video.minimum_size.x/videoSize.x
	else:
		scale = $background/video_frame/video.minimum_size.y/videoSize.y
	$background/video_frame/video.scale = Vector2(scale, scale)
	videoSize *= Vector2(scale, scale)
	$background/video_frame/video.position = $background/video_frame.minimum_size/2 - videoSize/2

func _on_done_pressed():
	queue_free()

func _on_play_pressed():
	$background/video_frame/video.play()
