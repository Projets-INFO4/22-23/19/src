extends Control

# Listen and choose card

var word : Word
var currentLayout : Control

signal card_pressed(card)

func setUpCard(_word : Word, difficulty : int, size : Vector2, horiz: bool) -> void:
	
	#display the right layout depending checked the difficulty 
	#print("layout_" + String(difficulty))
	#print(find_child("layout_" + String(difficulty)))
	#print($CardButton.get_children())
	word = _word
	var isBorelNeeded = true
	if len(word.iconPath) == 0:
		#Update view if the word don't have an image
		if difficulty == 1:
			difficulty = 3
		elif difficulty == 2:
			difficulty = 4
			isBorelNeeded = false
			
	currentLayout = find_child("layout_" + str(difficulty))
	currentLayout.visible = true
	
	#set the paramter of the card
	currentLayout.find_child("word").text = _word.word
	if len(word.iconPath) > 0:
		currentLayout.find_child("picture").texture = Global.load_icon(word.iconPath)
	if horiz:
		currentLayout.find_child("borel_maisonny_container").minimum_size.x = size.x/2
		currentLayout.find_child("borel_maisonny_container").minimum_size.y = size.y*0.87
	else:
		currentLayout.find_child("borel_maisonny_container").minimum_size.x = size.x*0.85
		currentLayout.find_child("borel_maisonny_container").minimum_size.y = size.y/3
	#set the card min size
	self.minimum_size = size
	
	#add the borel maisonny images to the card
	if isBorelNeeded:
		extract_borel_maisonny()


func _on_CardButton_pressed():
	emit_signal("card_pressed",self)
	
#This function extract all the borel maisonny sign picture out of all the
#phonetic symbol of our function
func extract_borel_maisonny() -> void:
	for imgPath in Global.phonetic_to_array_picture_path(word.phonetic):
		add_borel_maisonny(currentLayout.find_child("borel_maisonny_container"), imgPath)

#This function add a picture of a phonetic symbole in borel maisonny sign to
#a given container. Before adding the picture, the function resize all the other
#picture present in the container.
func add_borel_maisonny(container: HBoxContainer, imgPath: String) -> void:
	var newBorelMaisonny = TextureRect.new()
	newBorelMaisonny.expand = true
	newBorelMaisonny.stretch_mode = newBorelMaisonny.STRETCH_KEEP_ASPECT_CENTERED
	newBorelMaisonny.texture = load(imgPath)
	newBorelMaisonny.size_flags_horizontal = newBorelMaisonny.SIZE_SHRINK_CENTER
	var numberOtherBorelMaisonnyInContainer = container.get_child_count() + 1
	var maxSizeX = container.minimum_size.x/numberOtherBorelMaisonnyInContainer
	var correctSize = min(maxSizeX, container.minimum_size.y)
	newBorelMaisonny.minimum_size.x = correctSize
	newBorelMaisonny.minimum_size.y = correctSize
	for otherBorelMaisonny in container.get_children():
		otherBorelMaisonny.minimum_size.x = correctSize
		otherBorelMaisonny.minimum_size.y = correctSize
	container.add_child(newBorelMaisonny)

