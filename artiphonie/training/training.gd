extends Control

# Training scene for artiphonie

var pronouncingRessource := load(Global.artiphonie.PATH_PRONOUNCING)

func _ready():
	setup()

func setup():
	#deal with the instruction	
	var instruction = $Instruction
	instruction.setUp("train")
	
	if Global.player.get_Online():
		var http_request = HTTPRequest.new()
		add_child(http_request)
		http_request.connect("request_completed",Callable(self,"_http_request_completed_filter"))
		var body = {"id_enfant": int(Global.player.get_idPlayer())}
		var query=JSON.stringify(body)
		var error = http_request.request(Global.backendAddr + "/enfants/work?enfant_id=" + str(Global.player.get_idPlayer()), [], HTTPClient.METHOD_GET, query)
	else:
		var cmpt = 0
		var content = Global.load_only_json("user://mots.json")
		for word in content["mots"]:
			var newPronouncing = pronouncingRessource.instantiate()
			var mot = Word.new().from_dictionary_new(word)
			newPronouncing.setup(mot, 1, 'train')
			$ScrollContainer/words.add_child(newPronouncing)
			cmpt += 1
	
func _http_request_completed_filter(result, response_code, headers, body):
	if response_code==200:
		var test_json_conv = JSON.new()
		test_json_conv.parse(body.get_string_from_utf8())
		var dictionnaryResult = test_json_conv.get_data()
		Global.player.set_work(String(dictionnaryResult["wordList"]))
		Global.player.set_isWorking(true)
		
		var cmpt = 0
		var arrayWork = []
		var stringWork = Global.player.get_work()
		var arrayWorkPool = stringWork.rsplit(",", true)
		for item in arrayWorkPool:
			arrayWork.append(String(item))
			#arrayWork.append(item)
		for word in Global.get_word_training():
			var newPronouncing = pronouncingRessource.instantiate()
			newPronouncing.setup(word, 1, 'train')
			$ScrollContainer/words.add_child(newPronouncing)
			cmpt += 1
	else:
		var cmpt = 0
		var content = Global.load_only_json("user://mots.json")
		for word in content["mots"]:
			var newPronouncing = pronouncingRessource.instantiate()
			var mot = Word.new().from_dictionary_new(word)
			newPronouncing.setup(mot, 1, 'train')
			$ScrollContainer/words.add_child(newPronouncing)
			cmpt += 1
