extends Control

# Goose game

const NB_TILE: int = 10

var pronouncingRessource := load(Global.artiphonie.PATH_PRONOUNCING)
var pronouncingLayout: int
var pronouncingScene: Control

var currentTile :int = 1
var tileWords: Array

var maxAttempt: int
var attempt: int
var attemptPerWord: Array = []
var difficulty: String

var idFiltre : int = -1

func _ready():
	#deal with the instruction
	var instruction = $Instruction
	instruction.setUp("gooseGame")

	var arguments = Global.get_arguments()
	difficulty = arguments[0]
	setup()

func setup():
	print('setup')
	Global.player.game_nbAttempts = 0
	match difficulty:
		"Facile":
			pronouncingLayout = 1
			maxAttempt = 5
		"Normal":
			pronouncingLayout = 3
			maxAttempt = 2
		"Difficile":
			pronouncingLayout = 4
			maxAttempt = 1
	#maxAttempt += 1
	attempt = maxAttempt
	
	if Global.player.get_Online():
		print('Online')
		var http_request = HTTPRequest.new()
		add_child(http_request)
		http_request.connect("request_completed",Callable(self,"_http_request_completed_filter"))
		var body = {"id_enfant": int(Global.player.get_idPlayer())}
		var query=JSON.stringify(body)
		var error = http_request.request(Global.backendAddr + "/enfants/work?enfant_id=" + str(Global.player.get_idPlayer()), [], HTTPClient.METHOD_GET, query)
		if error != OK:
			push_error("An error occurred in the HTTP request.")
	else:
		#tileWords = Global.get_n_word_from_active_list(NB_TILE)
		print('Classic')
		tileWords = Global.get_n_word_from_dico(NB_TILE)
		move_player_to_next_position()
		attemptPerWord.resize(NB_TILE)

func _http_request_completed_filter(result, response_code, headers, body):
	print('response')
	if response_code==200:
		var test_json_conv = JSON.new()
		test_json_conv.parse(body.get_string_from_utf8())
		var dictionnaryResult = test_json_conv.get_data()
		Global.player.set_work(String(dictionnaryResult["wordList"]))
		Global.player.set_isWorking(true)
		idFiltre = dictionnaryResult["id"]
	else:
		Global.player.set_isWorking(false)
	#tileWords = Global.get_n_word_from_active_list(NB_TILE)
	print('Http')
	tileWords = Global.get_n_word_from_dico(NB_TILE)
	move_player_to_next_position()
	attemptPerWord.resize(NB_TILE)

func get_player_position_on_tile(tileNumber: int) -> Vector2:
	var tile: TextureRect = find_child("tile_" + str(tileNumber))
	var tileCenter := Vector2(tile.position.x + tile.size.x/2 , tile.position.y + tile.size.y/2)
	return  Vector2(tileCenter.x - $player.pivot_offset.x, tileCenter.y - $player.pivot_offset.y)

func _on_Validate_pressed():
	find_child("Validate").disabled = true
	pronouncingScene = pronouncingRessource.instantiate()
	pronouncingScene.setup(tileWords[currentTile-1], pronouncingLayout)
	pronouncingScene.connect("pronounced",Callable(self,"_on_pronounced"))
	add_child(pronouncingScene) # Add a child
	move_child(pronouncingScene, get_index()) # Move the child to the same level at his parent

func _on_pronounced(word: Word, result: bool):
	if not result:
		attempt -= 1
		if attempt > 0:
			return
	if result:
		$correct.playing = true
	pronouncingScene.queue_free()
	attemptPerWord[currentTile-1]=maxAttempt-attempt
	attempt = maxAttempt
	# if record : Global.speechToText.stopListen()
	if(currentTile == NB_TILE):
		end_game()
	currentTile += 1
	move_player_to_next_position()

func move_player_to_next_position() -> void:
	if currentTile != NB_TILE+1:
		var target = get_player_position_on_tile(currentTile)
		$tween.interpolate_property($player, "position", $player.position, target, 1, Tween.TRANS_QUINT, 0)
		$tween.start()

func _on_tween_tween_all_completed():
	find_child("Validate").disabled = false

func calculate_score():
	var sum = 0.0
	for element in attemptPerWord:
		sum += element
	var score = int(5.0 - float(sum/4.0))
	if score<=0:
		score = 1
	match difficulty:
		"Normal":
			score+=2
		"Difficile":
			score+=4
	return score


func end_game():
	Global.player.saveModification += 1
	var score = calculate_score()
	#args to send to game_end
	var args : Array = []
	var attemptDic = {}
	var timer = 0
	args.append("res://artiphonie/goose_game/goose_game.tscn")
	args.append([difficulty])
	args.append("Jeu de l'oie")
	args.append(difficulty)
	args.append(score)
	args.append(timer)
	for i in NB_TILE:
		attemptDic[tileWords[i].id] = attemptPerWord[i]
	args.append(attemptDic)
	args.append(idFiltre)
	args.append(unlock_difficulty())

	Global.change_scene_to_file("res://shared/game_end/game_end.tscn", args)


func unlock_difficulty():
	var success_percentage : float = float(NB_TILE) / float(Global.player.game_nbAttempts)
	var difficulty_to_unlock : String
	match difficulty:
		"Facile":
			difficulty_to_unlock = "Normal"
		"Normal":
			difficulty_to_unlock = "Difficile"
	var unlocksomething = false
	if success_percentage >= 0.8 :
		match difficulty_to_unlock :
			"Normal":
				if Global.player.locked_goose_medium:
					Global.player.locked_goose_medium = false
					unlocksomething = true
			"Difficile":
				if Global.player.locked_goose_hard:
					Global.player.locked_goose_hard = false
					unlocksomething = true
	return {"unlock": unlocksomething, "pourcentage": success_percentage}
