extends Control

# Avatar space is the place where the player can buy and equiped items

var ItemTypeScene := preload("./item_type.tscn")
var ItemScene := preload("./item.tscn")
var shop : Shop
var itemBying : Control

func _ready():
	if Global.listen_horiz:
		$ecouteForme.text = "Horizontal"
	else:
		$ecouteForme.text = "Vertical"
	$playerName.text = Global.player.get_player_name()
	$Stars/StarsNumber.text = str(Global.player.get_stars())
	$PlayerName.text = Global.player.get_player_name()
	
	$ScrollContainer2.visible = false

	shop = Shop.new()
	shop = Global.load_json(shop, "res://data/shopItem.json")

	checkItemsUnlock()
	add_item_type()
	add_item()


#Add the item type button checked top of the page
func add_item_type() -> void:
	var typeAlreadyIn : Array = []
	for itemArray in shop.sortedItems.values():
		if (itemArray[0] != null):
			var item = itemArray[0]
			var newItemType = ItemTypeScene.instantiate()
			newItemType.setUp(item)
			newItemType.connect("item_type_button_pressed",Callable(self,"_item_type_signal_received"))
			$HScrollBar/ItemType.add_child(newItemType)
			typeAlreadyIn.append(item.itemType)

			var newNode = GridContainer.new()
			newNode.name = item.itemType
			newNode.set_columns(3)
			find_child("ScrollContainer2").add_child(newNode)
			newNode.visible = false
			newItemType.nodeShop = newNode

#Add the item button to the respective type panel
func add_item() -> void:
	for itemArray in shop.sortedItems.values():
		for item in itemArray:
			var newItem = ItemScene.instantiate()

			newItem.connect("item_button_pressed",Callable(self,"_item_signal_received"))
			$ScrollContainer2.get_node(item.itemType).add_child(newItem)
			newItem.setUp(item)

#function called when a button of an item type is pressed
#this set invisible all the panel of item
func _item_type_signal_received():
	$ScrollContainer2.visible = true
	for child in $ScrollContainer2.get_children():
		child.visible = false

func _item_signal_received(itemScene :Control):
	if Global.player.get_stars() >= itemScene.item.required:
		Global.player.add_equiped_item(itemScene.item)
		updateShopVu()
		$profilPicture.update()
	else:
		#we check that the player have enough stars
		if itemScene.item.required > Global.player.get_stars():
			$NotEnoughStars.visible = true
	#the player has already bough this item
	#then it can equip it

func _on_Validate_pressed():
	$Stars/StarsNumber.text = str(Global.player.get_stars())
	#reset the button aspect
	itemBying.find_child("Star").visible = false
	itemBying.find_child("Required").visible = false
	itemBying.find_child("Equiped").visible = true
	#add item to unlocked item and equiped it
	Global.player.add_unlocked_item(itemBying.item)
	Global.player.add_equiped_item(itemBying.item)
	updateShopVu()
	$profilPicture.update()

func _on_Ok_pressed():
	$NotEnoughStars.visible = false

func _on_ValidateName_pressed():
	Global.player.set_player_name($playerName.text)

#need to be called when the profil picture is updated
#update the world "equiped" to the button related to equiped items
func updateShopVu():
	for containerType in $ScrollContainer2.get_children():
		for i in containerType.get_children():
			if Global.player.has_equiped_item(i.item):
				i.find_child("Equiped").visible = true
			else:
				i.find_child("Equiped").visible = false

func checkItemsUnlock() :
	for itemArray in shop.sortedItems.values():
		for item in itemArray:
			if(item.required <= Global.player.get_stars() && not Global.player.unlockedItems.has(item)) :
				Global.player.add_unlocked_item(item)


func _on_Button_pressed():
	if Global.player.get_gender() == 1:
		Global.player.set_gender(2)
	else:
		Global.player.set_gender(1)
	$profilPicture.update()


func _on_Button2_pressed():
	if Global.get_listen_horiz():
		Global.set_listen_horiz(false)
		$ecouteForme.text = "Vertical"
	else:
		Global.set_listen_horiz(true)
		$ecouteForme.text = "Horizontal"
	Global.player.saveModification += 1
