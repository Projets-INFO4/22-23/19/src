extends Control

# Scene which display an item and its star rating required

signal item_button_pressed(Control)
var item : Item

func setUp(_item : Item):
	item = _item
	find_child("ItemImage").texture = load("res://art/shopImages/"+item.picturePath)
	match Global.player.gender:
		1:
			$Button/Head.texture = load("res://art/avatar/boy"+str(Global.player.ethnicity)+".png")
		2:
			$Button/Head.texture = load("res://art/avatar/girl"+str(Global.player.ethnicity)+".png")
	var alreadyHas : bool = false
	for i in Global.player.unlockedItems:
		if item.equals(i):
			alreadyHas = true
	if not alreadyHas:		
		find_child("Required").text = str(item.required)
	else:
		find_child("Star").visible = false

func _on_Button_pressed():
	emit_signal("item_button_pressed", self)

