extends Control

# Phonetic element is a button, see phonetic page

var phoneticDemonstrationRessource := load("res://artiphonie/learning/phonetic_demonstration/phonetic_demonstration.tscn")
var phoneticName: String

func setup(_phoneticName: String):
	phoneticName = _phoneticName
	$button.text = "[" + Global.convert_phonetic(phoneticName) + "]"

func _on_button_pressed():
	var newPhoneticDemonstration: Control = phoneticDemonstrationRessource.instantiate()
	var picPath = ""
	var vidPath = ""
	if Global.phonetic_to_array_picture_path(phoneticName).size() > 0:
		picPath = Global.phonetic_to_array_picture_path(phoneticName)[0]
	if Global.phonetic_to_array_video_path(phoneticName).size() > 0:
		vidPath = Global.phonetic_to_array_video_path(phoneticName)[0]
	newPhoneticDemonstration.setup(phoneticName,
	picPath, vidPath)
	get_viewport().add_child(newPhoneticDemonstration)
